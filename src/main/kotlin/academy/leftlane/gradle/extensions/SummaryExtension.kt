package academy.leftlane.gradle.extensions

import org.gradle.api.Action
import org.gradle.api.model.ObjectFactory
import javax.inject.Inject

open class SummaryExtension @Inject constructor(
    private val objectFactory: ObjectFactory
) {
    var enabled = true
    val modulesContainerInstance: ModulesContainer = objectFactory.newInstance(ModulesContainer::class.java)

    fun modules(action: Action<ModulesContainer>) {
        action.execute(modulesContainerInstance)
    }

    open class Module {
        var name: String = ""
        var reportPath: String = ""
    }

    open class ModulesContainer @Inject constructor(
        private val objectFactory: ObjectFactory
    ) {
        val namesAndReports: MutableList<Module> = mutableListOf()

        fun module(action: Action<Module>) {
            val moduleInstance: Module = objectFactory.newInstance(Module::class.java)
            action.execute(moduleInstance)
            namesAndReports.add(moduleInstance)
        }
    }
}
