package academy.leftlane.gradle.tasks

import academy.leftlane.gradle.extensions.MetricsExtension
import org.dom4j.io.SAXReader
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import javax.inject.Inject

open class MetricsTask @Inject constructor(
    private val extension: MetricsExtension
) : DefaultTask() {

    @TaskAction
    fun extractTestReport() {
        if (!extension.enabled) return

        val includedCoverageTypes = extension.coverageTypes.map {
            it.toLowerCase()
        }

        for (module in extension.modulesContainerInstance.namesAndReports) {
            val reportPath = module.reportPath
            val document = SAXReader().read(File(reportPath))
            val coverageTypes = document.rootElement.elements("counter")

            for (coverageType in coverageTypes) {
                val coverageTypeName = coverageType.attribute("type").value.toLowerCase().capitalize()
                if (!includedCoverageTypes.contains(coverageTypeName.toLowerCase())) continue

                val covered = coverageType.attribute("covered").value.toFloat()
                val missed = coverageType.attribute("missed").value.toFloat()
                val percentage = (covered / (covered + missed)) * 100f

                println("${module.name} $coverageTypeName - ${"%.2f".format(percentage)}%")
            }

            println()
        }
    }
}
