package academy.leftlane.gradle.plugins

import academy.leftlane.gradle.extensions.MetricsExtension
import academy.leftlane.gradle.extensions.SummaryExtension
import academy.leftlane.gradle.tasks.MetricsTask
import academy.leftlane.gradle.tasks.SummaryTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class JacocoTestCoverageReporterPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        val metricsExtension = project.extensions.create("jacocoTestMetrics", MetricsExtension::class.java)
        project.tasks.create("extractMetrics", MetricsTask::class.java, metricsExtension)

        val summaryExtension = project.extensions.create("jacocoTestSummary", SummaryExtension::class.java)
        project.tasks.create("extractSummary", SummaryTask::class.java, summaryExtension)
    }
}
