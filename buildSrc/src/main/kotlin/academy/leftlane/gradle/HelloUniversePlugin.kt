package academy.leftlane.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class HelloUniversePlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.tasks.create("writeMessageToUniverse", MessageTask::class.java)
    }
}
